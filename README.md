## MacOS:
## Prerequisites Software/Tools
 - Git [ http://git-scm.com/downloads ]

 - Java 8 or Java 9 [http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html]

 - Cucumber [https://cucumber.io/]

 - Gherkin [https://cucumber.io/docs/reference]

 - Gradle [https://gradle.org/]

 - Selenide [http://selenide.org/]

 - some IDE [I prefer IntelliJ IDEA]

## Getting Started

1. Install GIT:
There are several ways to install Git on a Mac. The easiest is probably to install the Xcode Command Line Tools. On Mavericks (10.9) or above you can do this simply by trying to run git from the Terminal the very first time.

$ git --version
If you don’t have it installed already, it will prompt you to install it.

If you want a more up to date version, you can also install it via a binary installer. A macOS Git installer is maintained and available for download at the Git website, at http://git-scm.com/download/mac.

Git macOS installer.
You can also install it as part of the GitHub for Mac install. Their GUI Git tool has an option to install command line tools as well. You can download that tool from the GitHub for Mac website, at http://mac.github.com.

2. Install Java: https://java.com/en/download/help/mac_install.xml

3. Install Gradle:

 - brew install gradle

## Then:

4. Open the terminal / console and Initialize the project using GIT; then clone the project using below command: git clone https://dimencionrider@bitbucket.org/dimencionrider/atdd-framework.git

5. Once the project is cloned successfully open the terminal and navigate to the project directory "atdd-framework".
6.  Run the below command to start the tests:

 -  gradle cucumber
