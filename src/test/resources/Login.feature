Feature: LoginFeature
    This feature deals with the login functionality of the application


  Scenario: Login with correct username and password
      Given user navigate to the main page
      When user enter the username username and password password
      And user click submit button
      Then user should see the home page


  Scenario: Login with correct username and incorrect password
      Given user navigate to the main page
      When user enter the username rshayda and invalid password @@@000
      And user click submit button
      Then user should see the error message