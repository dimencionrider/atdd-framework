package gradle.cucumber;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gradle.cucumber.pages.HomePage;
import gradle.cucumber.pages.MainPage;

public class LoginSteps {

    MainPage mainPage = new MainPage();
    HomePage homePage = new HomePage();

    @Given("user navigate to the main page$")
    public void userNavigateToTheMainPage() throws Throwable {
        mainPage.open();
    }

    @When("user enter the username (.*) and password (.*)$")
    public void userEnterUsernameAdminAndPasswordAdmin(String login, String password) throws Throwable {
        mainPage.login(login, password);
    }

    @And("user click submit button$")
    public void userClickLoginButton() throws Throwable {
        mainPage.clickOnSubmitBtn();
    }

    @Then("user should see the home page$")
    public void userShouldSeeTheMailPage() throws Throwable {
        homePage.checkOpenHomePage();
    }
}
