package gradle.cucumber.pages;

import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;

public class Base {

    @Before
    public void setUp() {
        ChromeDriverManager.getInstance().setup();
        com.codeborne.selenide.Configuration.browser = "chrome";
    }
}
