package gradle.cucumber.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MainPage {

    public SelenideElement txtUserName = $(By.name("LoginForm[username]"));
    public SelenideElement txtPassword = $(By.name("LoginForm[password]"));
    public SelenideElement btnLogin = $(By.name("yt0"));

    public MainPage open() {
        Selenide.open("http://internal.lv.remit.se/login");
        return this;
    }

    public void login(String userName, String password) {
        txtUserName.val(userName);
        txtPassword.val(password);
    }

    public void clickOnSubmitBtn() {
        btnLogin.submit();
    }

}
