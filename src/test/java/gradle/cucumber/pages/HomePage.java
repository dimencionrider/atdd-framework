package gradle.cucumber.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class HomePage {

    public SelenideElement newMessageBtn = $("#yw0 > li:nth-child(2) > a");

    public void checkOpenHomePage() {
        newMessageBtn.shouldHave(Condition.text("My vacations"));
    }
}
